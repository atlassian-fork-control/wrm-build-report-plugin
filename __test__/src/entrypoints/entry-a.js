export default () => {
    console.log('Hello entry point A');
    return import(/* webpackChunkName: "lib-a" */ './libs/lib-a').then(({ default: sayHello }) => sayHello());
};