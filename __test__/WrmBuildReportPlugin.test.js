const path = require('path');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');
const merge = require('webpack-merge');
const fs = require('fs');

const BuildReportPlugin = require('../src/WrmBuildReportPlugin');

const DEFAULT_REPORT_FILE_PATH = path.join(__dirname, 'tmp', './report.json');
const DEFAULT_BASE_URL = 'https://bulldog.internal.atlassian.com/';

const getWebpackConfiguration = (reportOptions) => {
    return {
        entry: {
            main: path.resolve(__dirname, 'src', 'index.js'),
        },

        output: {
            path: path.join(__dirname, 'tmp', 'dist'),
            filename: '[name].bundle.js',
            chunkFilename: '[name].bundle.js',
        },

        plugins: [
            new CleanWebpackPlugin(),
            new WrmPlugin({
                pluginKey: 'com.atlassian.plugins.jira.wrm-report-test',
                xmlDescriptors: path.join(__dirname, 'tmp', 'META-INF', 'plugin-descriptors', 'wr-defs.xml')
            }),
            new BuildReportPlugin(reportOptions)
        ]
    };
}

describe('WrmBuildReportPlugin', () => {
    test('default setup', done => {
        const config = getWebpackConfiguration({
            filename: DEFAULT_REPORT_FILE_PATH
        });

        webpack(config, (err, stats) => {
            expect(err).toBe(null);
            expect(stats.hasErrors()).toBe(false);

            const reportContent = fs.readFileSync(DEFAULT_REPORT_FILE_PATH, 'utf-8');

            expect(reportContent).toMatchSnapshot();

            done();
        });
    });

    test('with "externals" flag', done => {
        const config = getWebpackConfiguration({
            filename: DEFAULT_REPORT_FILE_PATH,
            baseUrl: DEFAULT_BASE_URL,
            externals: true
        });

        webpack(config, (err, stats) => {
            expect(err).toBe(null);
            expect(stats.hasErrors()).toBe(false);

            const reportContent = fs.readFileSync(DEFAULT_REPORT_FILE_PATH, 'utf-8');

            expect(reportContent).toMatchSnapshot();

            done();
        });
    });

    test('with "sizeOnly" flag', done => {
        const config = getWebpackConfiguration({
            filename: DEFAULT_REPORT_FILE_PATH,
            sizeOnly: true
        });

        webpack(config, (err, stats) => {
            expect(err).toBe(null);
            expect(stats.hasErrors()).toBe(false);

            const reportContent = fs.readFileSync(DEFAULT_REPORT_FILE_PATH, 'utf-8');
            expect(reportContent).toMatchSnapshot();

            done();
        });
    });

    test('with "staticReport" flag', done => {
        const staticReportFilePath = path.join(__dirname, 'tmp', 'static-report.html');

        const config = getWebpackConfiguration({
            filename: DEFAULT_REPORT_FILE_PATH,
            baseUrl: DEFAULT_BASE_URL,
            staticReport: true,
            staticReportFilename: './__test__/tmp/static-report.html'
        });

        webpack(config, (err, stats) => {
            expect(err).toBe(null);
            expect(stats.hasErrors()).toBe(false);

            const staticReportStat = fs.statSync(staticReportFilePath);

            expect(staticReportStat.isFile()).toBe(true);

            done();
        });
    });

    test('with async chunks', done => {
        const staticReportFilePath = path.join('.', '__test__', 'tmp', 'static-report.html');
        const config = getWebpackConfiguration({
            filename: DEFAULT_REPORT_FILE_PATH,
            baseUrl: DEFAULT_BASE_URL,
            staticReport: true,
            staticReportFilename: staticReportFilePath
        });

        webpack(merge(config, {
            entry: {
                entryA: path.resolve(__dirname, 'src', 'entrypoints', 'entry-a.js'),
                entryB: path.resolve(__dirname, 'src', 'entrypoints', 'entry-b.js'),
            },
            optimization: {
                splitChunks: {
                    chunks: 'all'
                }
            }
        }), (err, stats) => {
            expect(err).toBe(null);
            expect(stats.hasErrors()).toBe(false);

            const reportContent = fs.readFileSync(DEFAULT_REPORT_FILE_PATH, 'utf-8');

            expect(reportContent).toMatchSnapshot();

            done();
        });
    });
});